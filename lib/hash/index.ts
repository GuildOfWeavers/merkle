// IMPORTS
// ================================================================================================
import { HashAlgorithm, HashFunction } from '@gow/merkle';
import { sha256 } from './sha256';

// PUBLIC FUNCTIONS
// ================================================================================================
export function getHashFunction(algorithm: HashAlgorithm): HashFunction {
    switch (algorithm) {
        case HashAlgorithm.sha256: {
            return sha256;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
}