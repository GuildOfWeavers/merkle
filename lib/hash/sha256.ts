// IMPORTS
// ================================================================================================
import * as crypto from 'crypto';

// MODULE VARIABLES
// ================================================================================================
const MAX_VALUE = 2n**256n;

const buf1 = Buffer.alloc(32);
const buf2 = Buffer.alloc(64);

// PUBLIC FUNCTIONS
// ================================================================================================
export function sha256(v1: bigint, v2?: bigint): bigint {
    if (v1 > MAX_VALUE) throw new TypeError('Value must be 256 bits or smaller');
    const hex1 = v1.toString(16).padStart(64, '0');
    if (v2 === undefined) {
        buf1.write(hex1, 0, 32, 'hex');
        const hash = crypto.createHash('sha256');
        hash.update(buf1);
        return BigInt('0x' + hash.digest().toString('hex'));
    }
    else {
        if (v2 > MAX_VALUE) throw new TypeError('Second value must be 256 bits or smaller');
        const hex2 = v2.toString(16).padStart(64, '0');
        buf2.write(hex1, 0, 32, 'hex');
        buf2.write(hex2, 32, 32, 'hex');
        const hash = crypto.createHash('sha256');
        hash.update(buf2);
        return BigInt('0x' + hash.digest().toString('hex'));
    }
}