declare module '@gow/merkle' {

    // HASHING
    // --------------------------------------------------------------------------------------------
    export const enum HashAlgorithm {
        sha256 = 1
    }

    export interface HashFunction {
        (v1: bigint, v2?: bigint): bigint;
    }

    export function getHashFunction(algorithm: HashAlgorithm): HashFunction;

    // MERKLE TREE
    // --------------------------------------------------------------------------------------------
    export interface BatchProof {
        values  : bigint[];
        nodes   : bigint[][];
        depth   : number;
    }
    
    export class MerkleTree {
        static create(values: bigint[], hash?: HashAlgorithm): MerkleTree;
        static createAsync(values: bigint[], hash?: HashAlgorithm): Promise<MerkleTree>;

        readonly root: bigint;

        prove(index: number): bigint[];
        proveBatch(indexes: number[]): BatchProof;

        static verify(root: bigint, index: number, proof: bigint[], hash?: HashAlgorithm): boolean;
        static verifyBatch(root: bigint, indexes: number[], proof: BatchProof, hash?: HashAlgorithm): bigint[] | undefined;
    }

    // MERKLE MAP
    // --------------------------------------------------------------------------------------------
    export class MerkleMap {

        static create(depth: number, data: Map<bigint,bigint>): MerkleMap;
        static createAsync(depth: number, data: Map<bigint,bigint>): Promise<MerkleMap>;

        readonly root: bigint;

        get(key: bigint): bigint | undefined;
        set(key: bigint, value: bigint): bigint[];
        prove(key: bigint): bigint[];

        static update(root: bigint, key: bigint, newValue: bigint, proof: bigint[]): bigint;
        static verify(root: bigint, key: bigint, proof: bigint[]): boolean;
    }

    // MERKLE ACCUMULATOR
    // --------------------------------------------------------------------------------------------
    export class MerkleAccumulator {

        readonly roots: (bigint[] | null)[];

        add(value: bigint): bigint[];
        
        static verify(proof: bigint[]): boolean;
    }
    
}